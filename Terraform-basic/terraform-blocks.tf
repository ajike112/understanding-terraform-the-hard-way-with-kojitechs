## this block is meant to set constraint on terraform version
terraform {
    required_version = ">= 1.1.0"


  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}


##second aPP


## Best approach for provider block
provider "aws" {
    region = "us-east-1"
    profile = "kunleajike"
}

## Resource Block (create resource/bring into existence)

resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16" # cidr (re-usable) # variable
  instance_tenancy = "default"

  tags = {
    Name = "main"
  }
}

### variables (types of variables....) # string
# '' # is a single string
# "" # is a double string
# You should be descriptive in naming your variables

#To create a variable 
variable "ami_id" {
    type = string
    description ="ami_id"
    default = "ami-09d3b3274b6c5d4aa"
}



##Creating EC2   
         #local_name        # resource_name
resource "aws_instance"      "ec2_instance" {
  ami           = var.ami_id
  instance_type = "t2.micro"

tags = {
    Name = "ec2_instance"
    
}
  }

##